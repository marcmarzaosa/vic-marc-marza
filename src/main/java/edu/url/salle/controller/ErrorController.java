package edu.url.salle.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Controller
public class ErrorController {
	
	@ExceptionHandler(Exception.class)
	public ModelAndView mostrarExcepcio(){
		return new ModelAndView("error/errorGeneral");
	}
	

}
